<h1 align="center">Red Light, Green Light</h1>

<h6 align="center">
Progressive Web App code challenge made with Javascript and React.js
</h6>


## Description
Welcome to my project! This is a Red Light, Green Light Progressive Web App Game made in Javascript and React.js using React Testing Library and Jest for testing suits and eslint/prettier for code formatting. It allows users to play the game and persist each user points into a rank. 

### Instructions

You need to have node.js and npm installed with a preferable LTS version.

First, clone the repo:

```bash
> git clone https://gitlab.com/luchosr/red-light-green-light-pwa.git
```


 once you cloned the repository and cd into it yo will need to install the dependencies


```bash
> npm install
```

with all the dependencies correctly installed, now you can run the app


```bash
> npm start
```

then you will need to open a web browser tab with the following URL:

  Local:            http://localhost:3000
  Or on Your Network:  http://172.24.64.1:3000


If you want to run the test suites:

```bash
> npm test
```

Also, you can access the PWA deployment [HERE](https://656f41909eb9700d456be703--grand-tanuki-b8c0ba.netlify.app/).




