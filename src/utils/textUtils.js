// Home view text variables
export const HOME_MAIN_TITLE = 'Create new player or log in';
export const VALIDATION_ERROR_MESSAGE =
	'Incorrect format, player names can only contain letters and/or numbers';
export const HOME_REGISTER_BUTTON_TEXT = 'Register';

export const CHECK_RANK_BUTTON_TEXT = 'check player ranking';

// Game view  text variables
export const NAV_BAR_MAIN_TEXT = 'Hello';
export const GAME_VIEW_MAIN_TITLE = 'High Score:';
export const GAME_VIEW_MAIN_SUBTITLE = 'Score:';
export const GAME_VIEW_LEFT_BUTTON = 'Left';
export const GAME_VIEW_RIGHT_BUTTON = 'Right';

// Ranking view text variables
export const PLAYERS_RANK_MAIN_TITLE = 'Players Rank';
export const PLAYERS_RANK_TABLE_HEADINGS = [
	'Position',
	'Player Name',
	'High Score',
];
export const RANKING_BUTTON_TEXT = 'go back to home page';
export const RANKING_HASHTAG_TEXT = '#';

// Game Color Variables
export const RED_COLOR = '#FF0000';
export const GREEN_COLOR = '#008000';
export const MAIN_BACKGROUND_COLOR = '#fdfdfd';
export const MAIN_PRIMARY_COLOR = '#004481';

// Light interval time variables
export const GREEN_LIGHT_TIME_MAX_VALUE = 10000;
export const RED_LIGHT_TIME_VALUE = 3000;
export const GREEN_LIGHT_TIME_MIN_VALUE = 2000;
export const SCORE_VARIABLE_FACTOR = 100;
export const RANDOM_INTERVAL_MAX_VALUE = 1500;
export const RANDOM_INTERVAL_MIN_VALUE = -1500;
