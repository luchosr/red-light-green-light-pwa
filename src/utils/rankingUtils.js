export const transformArray = (originalArray) => {
	return originalArray.map(({ playerName, highScore }) => ({
		playerName,
		highScore,
	}));
};

export const orderByHighScoreDesc = (players) => {
	return players.sort((a, b) => b.highScore - a.highScore);
};

export const transformArray2 = (originalArray) => {
	return originalArray.sort((a, b) => b.highScore - a.highScore);
};
