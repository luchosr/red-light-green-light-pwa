import React from 'react';
import { render, screen } from '@testing-library/react';
import { Table } from './Table';

describe('Table component', () => {
	const headings = ['Rank', 'Player Name', 'High Score'];
	const players = [
		{ playerName: 'Player1', highScore: 10 },
		{ playerName: 'Player2', highScore: 15 },
		{ playerName: 'Player3', highScore: 12 },
	];

	it('renders headings and player data correctly', () => {
		render(<Table headings={headings} tableData={players} />);

		headings.forEach((heading) => {
			const headingElement = screen.getByText(heading);
			expect(headingElement).toBeInTheDocument();
		});

		players.forEach((player, index) => {
			const rankElement = screen.getByText(`${index + 1}`);
			const playerNameElement = screen.getByText(player.playerName);
			const highScoreElement = screen.getByText(`${player.highScore}`);

			expect(rankElement).toBeInTheDocument();
			expect(playerNameElement).toBeInTheDocument();
			expect(highScoreElement).toBeInTheDocument();
		});
	});
});
