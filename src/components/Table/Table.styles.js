import { MAIN_PRIMARY_COLOR } from '../../utils/textUtils';

export const sectionWrapper = {
	width: '80%',
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'center',
};

export const sectionWrapperTable = {
	border: `1px solid ${MAIN_PRIMARY_COLOR}`,
};

export const tableHeading = {
	width: '20%',
	textAlign: 'left',
	color: MAIN_PRIMARY_COLOR,
	border: `1px solid ${MAIN_PRIMARY_COLOR}`,
};

export const tableDataCell = { paddingLeft: '0.4rem' };
