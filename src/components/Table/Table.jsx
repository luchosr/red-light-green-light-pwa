import React from 'react';

import { PropTypes } from 'prop-types';

import { Box } from '@mui/material';

import * as styles from './Table.styles';

export const Table = ({ headings, tableData, optionalText }) => {
	return (
		<Box component="section" sx={styles.sectionWrapper}>
			<Box component="table" sx={styles.sectionWrapperTable}>
				<Box component="thead">
					<Box component="tr">
						{headings.map((heading, index) => (
							<Box key={index} component="th" sx={styles.tableHeading}>
								{heading}
							</Box>
						))}
					</Box>
				</Box>
				<Box component="tbody">
					{tableData.map((data, index) => (
						<Box component="tr" key={index}>
							<Box component="td" sx={styles.tableDataCell}>
								{optionalText}
								{index + 1}
							</Box>
							<Box component="td" sx={styles.tableDataCell}>
								{data.playerName}
							</Box>
							<Box component="td" sx={styles.tableDataCell}>
								{data.highScore}
							</Box>
						</Box>
					))}
				</Box>
			</Box>
		</Box>
	);
};

Table.propTypes = {
	headings: PropTypes.array,
	tableData: PropTypes.array,
	optionalText: PropTypes.string,
};
