import { MAIN_PRIMARY_COLOR } from '../../utils/textUtils';

export const wrapper = { flexGrow: 1 };

export const wrapperAppBar = { backgroundColor: MAIN_PRIMARY_COLOR };

export const toolbarHeading = { flexGrow: 1 };
