import React from 'react';
import { render, screen } from '@testing-library/react';
import { GameAppBar } from './GameAppBar';

describe('GameAppBar component', () => {
	it('renders without crashing', () => {
		const playerName = 'Luciano';
		render(<GameAppBar playerName={playerName} handleButtonClick={() => {}} />);
	});

	it('renders the component with player name', () => {
		const playerName = 'Luciano';
		render(<GameAppBar playerName={playerName} handleButtonClick={() => {}} />);

		expect(screen.getByText(`Hello ${playerName}`)).toBeInTheDocument();
	});

	it('renders the correct heading', () => {
		render(<GameAppBar />);
		const helloHeading = screen.getByText(/Hello/i);

		expect(helloHeading).toBeInTheDocument();
	});
});
