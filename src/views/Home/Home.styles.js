import {
	MAIN_BACKGROUND_COLOR,
	MAIN_PRIMARY_COLOR,
} from '../../utils/textUtils';

export const home = {
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'center',
};

export const homeHeader = {
	display: 'flex',
	flexDirection: 'column',
	mt: 3,
};

export const headerIconMouse = {
	margin: '0 auto',
	padding: '10px',
	backgroundColor: MAIN_PRIMARY_COLOR,
	color: MAIN_BACKGROUND_COLOR,
	borderRadius: '30px',
};

export const headerTitleMain = {
	fontSize: '1.5rem',
	textAlign: 'center',
	mt: 2,
	color: MAIN_PRIMARY_COLOR,
	fontWeight: '600',
};

export const homeForm = {
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'center',
	margin: ' 2rem 0 2rem 0',
};

export const formSubmitButton = {
	width: '60%',
	mt: 4,
};

export const checkoutRankButton = {
	width: '12rem',
	fontSize: '0.75rem',
	mt: 2,
};
