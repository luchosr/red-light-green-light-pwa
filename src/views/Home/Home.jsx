import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { TextField, Box, Button, Typography } from '@mui/material';

import MouseIcon from '@mui/icons-material/Mouse';

import { createNewPlayerOrLogIn, validateName } from '../../utils/gameUtils';

import {
	CHECK_RANK_BUTTON_TEXT,
	HOME_MAIN_TITLE,
	HOME_REGISTER_BUTTON_TEXT,
	VALIDATION_ERROR_MESSAGE,
} from '../../utils/textUtils';

import { getListOfPlayers } from '../../services/localStorageServices';

import * as styles from './Home.styles';

export const Home = () => {
	const [playerName, setPlayerName] = useState('');
	const [playersList, setPlayersList] = useState([]);
	const [error, setError] = useState({ error: false, message: '' });

	const navigate = useNavigate();

	useEffect(() => {
		const listOfPlayers = getListOfPlayers();

		if (listOfPlayers) {
			setPlayersList(listOfPlayers);
		}
	}, []);

	const handleSubmit = (e) => {
		e.preventDefault();

		if (!validateName(playerName)) {
			setError({
				error: true,
				message: VALIDATION_ERROR_MESSAGE,
			});

			return;
		}

		createNewPlayerOrLogIn(playerName, playersList, setPlayersList);

		navigate('/game');
	};

	return (
		<Box component="main" sx={styles.home}>
			<Box component="header" sx={styles.homeHeader}>
				<MouseIcon sx={styles.headerIconMouse} />
				<Typography sx={styles.headerTitleMain} variant="h5" component="h1">
					{HOME_MAIN_TITLE}
				</Typography>
			</Box>
			<Box component="form" sx={styles.homeForm} onSubmit={handleSubmit}>
				<TextField
					required
					fullWidth
					type="text"
					label="Name"
					id="playerName"
					variant="outlined"
					value={playerName}
					error={error.error}
					onChange={(e) => setPlayerName(e.target.value)}
					helperText={error.message}
				/>
				<Button sx={styles.formSubmitButton} type="submit" variant="outlined">
					{HOME_REGISTER_BUTTON_TEXT}
				</Button>
			</Box>
			<Button
				sx={styles.checkoutRankButton}
				type="button"
				variant="outlined"
				onClick={() => navigate('/rank')}
			>
				{CHECK_RANK_BUTTON_TEXT}
			</Button>
		</Box>
	);
};
