import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

import { Home } from './Home';

jest.mock('../../services/localStorageServices', () => ({
	getListOfPlayers: jest.fn(),
}));

describe('Home Component', () => {
	beforeEach(() => {
		window.localStorage.clear();
		jest.resetAllMocks();
	});

	test('renders Home component', () => {
		const { getByText, getByLabelText } = render(
			<MemoryRouter>
				<Home />
			</MemoryRouter>
		);

		expect(getByText(/Create new player or log in/i)).toBeInTheDocument();

		expect(getByLabelText(/Name/i)).toBeInTheDocument();
	});

	test('displays validation error message when submitting an invalid name', async () => {
		const { getByLabelText, getByText } = render(
			<MemoryRouter>
				<Home />
			</MemoryRouter>
		);

		fireEvent.change(getByLabelText(/Name/i), {
			target: { value: 'Invalid 123' },
		});
		fireEvent.click(getByText('Register'));

		await waitFor(() => {
			expect(
				getByText(
					'Incorrect format, player names can only contain letters and/or numbers'
				)
			).toBeInTheDocument();
		});
	});
});
