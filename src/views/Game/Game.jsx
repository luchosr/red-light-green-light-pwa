import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Box, Button, Typography } from '@mui/material';

import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import TrafficIcon from '@mui/icons-material/Traffic';

import { GameAppBar } from '../../components/GameAppBar/GameAppBar';

import {
	greenLightTimeCalculator,
	filterPlayersByName,
} from '../../utils/gameUtils';

import * as styles from './Game.styles';

import {
	GAME_VIEW_MAIN_TITLE,
	GAME_VIEW_MAIN_SUBTITLE,
	GAME_VIEW_LEFT_BUTTON,
	GAME_VIEW_RIGHT_BUTTON,
	RED_COLOR,
	GREEN_COLOR,
	RED_LIGHT_TIME_VALUE,
} from '../../utils/textUtils';

import {
	getListOfPlayers,
	getCurrentPlayer,
	persistListOfPlayers,
	persistCurrentPLayer,
} from '../../services/localStorageServices';

export const Game = () => {
	const [trafficLightColor, setTrafficLightColor] = useState(RED_COLOR);
	const [currentPlayerData, setCurrentPlayerData] = useState([]);
	const [matchScore, setMatchScore] = useState(0);
	const [playerHighScore, setPlayerHighScore] = useState(0);
	const [playersList, setPlayersList] = useState([]);
	const [previousStep, setPreviousStep] = useState('');

	const navigate = useNavigate();

	useEffect(() => {
		const listOfPlayers = getListOfPlayers();
		if (listOfPlayers) {
			setPlayersList(listOfPlayers);
		}

		const currentPlayer = getCurrentPlayer();

		if (currentPlayer) {
			setCurrentPlayerData(currentPlayer);
			setMatchScore(currentPlayer?.score);
			setPlayerHighScore(currentPlayer?.highScore);
		}
	}, []);

	useEffect(() => {
		const redTimer = setTimeout(() => {
			setTrafficLightColor(GREEN_COLOR);
		}, RED_LIGHT_TIME_VALUE);

		const myInterval = setInterval(() => {
			setTimeout(() => {
				setTrafficLightColor(GREEN_COLOR);
			}, RED_LIGHT_TIME_VALUE);
			setTrafficLightColor(RED_COLOR);
		}, greenLightTimeCalculator(matchScore));

		return () => {
			clearTimeout(redTimer);
			clearInterval(myInterval);
		};
	}, []);

	const handleButtonClick = (step) => {
		if (trafficLightColor === RED_COLOR) {
			setMatchScore(0);
			setPreviousStep(step);
			persistCurrentPLayer({
				playerName: currentPlayerData?.playerName,
				score: 0,
				highScore: playerHighScore,
			});
		} else {
			if (step !== previousStep && trafficLightColor === GREEN_COLOR) {
				setMatchScore((prevScore) => prevScore + 1);

				if (matchScore === playerHighScore)
					setPlayerHighScore(playerHighScore + 1);

				setPreviousStep(step);
				persistCurrentPLayer({
					playerName: currentPlayerData?.playerName,
					score: matchScore + 1,
					highScore: playerHighScore,
				});
			} else {
				setMatchScore((prevScore) => (prevScore > 0 ? prevScore - 1 : 0));
				setPreviousStep(step);
				persistCurrentPLayer({
					playerName: currentPlayerData?.playerName,
					score: matchScore - 1,
					highScore: playerHighScore,
				});
			}
		}
	};

	const handleButtonOnLogOut = (e) => {
		e.preventDefault();
		const currentPlayerName = currentPlayerData?.playerName;
		const filteredPlayers = filterPlayersByName(playersList, currentPlayerName);
		const updatedPlayer = {
			playerName: currentPlayerName,
			score: matchScore,
			highScore: playerHighScore,
		};
		const newArrayOfPlayers = [...filteredPlayers, updatedPlayer];
		persistListOfPlayers(newArrayOfPlayers);
		navigate('/');
	};

	return (
		<>
			<GameAppBar
				playerName={currentPlayerData?.playerName}
				handleLogOutButton={handleButtonOnLogOut}
			/>
			<Box component="main">
				<Box component="article" sx={styles.gameMainComponentArticle}>
					<Typography sx={styles.articleHeading} variant="h5" component="h1">
						{`${GAME_VIEW_MAIN_TITLE} ${playerHighScore}`}
					</Typography>
					<TrafficIcon
						sx={{ width: '8rem', height: '8rem', color: trafficLightColor }}
					/>
					<Typography variant="h5" component="h2">
						{`${GAME_VIEW_MAIN_SUBTITLE} ${matchScore}`}
					</Typography>
				</Box>
				<Box component="section" sx={styles.buttonWrapper}>
					<Button
						variant="outlined"
						startIcon={<ArrowUpwardIcon />}
						sx={styles.buttonWrapperButton}
						onClick={() => handleButtonClick('left')}
					>
						{GAME_VIEW_LEFT_BUTTON}
					</Button>
					<Button
						variant="outlined"
						startIcon={<ArrowUpwardIcon />}
						onClick={() => handleButtonClick('right')}
						sx={styles.buttonWrapperButton}
					>
						{GAME_VIEW_RIGHT_BUTTON}
					</Button>
				</Box>
			</Box>
		</>
	);
};
