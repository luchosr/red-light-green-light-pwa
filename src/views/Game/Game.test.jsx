import React from 'react';
import { render, screen } from '@testing-library/react';
import { Game } from './Game';

// Mocking the useNavigate hook
jest.mock('react-router-dom', () => ({
	...jest.requireActual('react-router-dom'),
	useNavigate: () => jest.fn(),
}));

jest.mock('../../services/localStorageServices.js');
jest.mock('../../utils/gameUtils.js');

describe('Game component', () => {
	test('renders component without errors', () => {
		render(<Game />);

		expect(screen.getByText(/High Score:/i)).toBeInTheDocument();
	});
});
