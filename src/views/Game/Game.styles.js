export const gameMainComponentArticle = {
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'center',
};

export const articleHeading = {
	mt: 3,
};

export const buttonWrapper = {
	display: 'flex',
	flexDirection: 'row',
	justifyContent: 'center',
	mt: 2,
};

export const buttonWrapperButton = { width: '35%' };
