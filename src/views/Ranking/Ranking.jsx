import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Box, Typography, Button } from '@mui/material';

import { Table } from '../../components/Table/Table';

import { getListOfPlayers } from '../../services/localStorageServices';

import { orderByHighScoreDesc } from '../../utils/rankingUtils';
import {
	PLAYERS_RANK_MAIN_TITLE,
	PLAYERS_RANK_TABLE_HEADINGS,
	RANKING_BUTTON_TEXT,
	RANKING_HASHTAG_TEXT,
} from '../../utils/textUtils';

import * as styles from './Ranking.styles';

export const Ranking = () => {
	const [playersList, setPlayersList] = useState();

	const navigate = useNavigate();

	useEffect(() => {
		const listOfPlayers = getListOfPlayers();
		if (listOfPlayers) {
			const formattedArrayOfPlayers = orderByHighScoreDesc(listOfPlayers);
			setPlayersList(formattedArrayOfPlayers);
		}
	}, []);

	return (
		<Box component="main" sx={styles.mainWrapper}>
			<Typography variant="h4" component="h1" sx={styles.mainWrapperHeading}>
				{PLAYERS_RANK_MAIN_TITLE}
			</Typography>
			{playersList && (
				<Table
					headings={PLAYERS_RANK_TABLE_HEADINGS}
					tableData={playersList}
					optionalText={RANKING_HASHTAG_TEXT}
				/>
			)}
			<Button
				sx={styles.mainNavigationButton}
				variant="outlined"
				onClick={() => navigate('/')}
			>
				{RANKING_BUTTON_TEXT}
			</Button>
		</Box>
	);
};
