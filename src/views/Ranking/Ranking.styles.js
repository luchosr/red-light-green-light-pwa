import { MAIN_PRIMARY_COLOR } from '../../utils/textUtils';

export const mainWrapper = {
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'center',
};

export const mainWrapperHeading = {
	mt: 2,
	mb: 2,
	fontSize: '2rem',
	color: MAIN_PRIMARY_COLOR,
};

export const mainNavigationButton = {
	marginTop: '3rem',
};
