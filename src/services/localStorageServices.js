export const persistCurrentPLayer = (currentPlayerDataObject) => {
	window.localStorage.setItem(
		'current-player',
		JSON.stringify([currentPlayerDataObject])
	);
};

export const persistListOfPlayers = (playersListArray) => {
	window.localStorage.setItem(
		'red-light-green-light-players',
		JSON.stringify(playersListArray)
	);
};

export const getListOfPlayers = () => {
	const listOfPlayers = JSON.parse(
		window.localStorage.getItem('red-light-green-light-players')
	);
	return listOfPlayers;
};

export const getCurrentPlayer = () => {
	const currentPlayer = JSON.parse(
		window.localStorage.getItem('current-player')
	);
	return currentPlayer[0];
};
